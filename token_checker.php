<?php

    function check_token()
    {
        if(!isset($_REQUEST['access_token']))
        {
            echo json_encode(array(
                'success'   => 0,
                'error'     => 'Please provide access_token!',
                'code'      => 1
            ));
            exit;
        }
        else
        {
            $db = getDB();
            $query = 'SELECT faculty_number FROM token_service.tokens WHERE token = ? AND faculty_number = ? AND now() - interval \'2 hours\' < granted';
            $checker = $db->prepare($query) or database_error();

            list($fn, $token) = explode('-', $_REQUEST['access_token']);

            $checker->execute(array($token, $fn)) or database_error();
            if ($checker->rowCount() == 0)
            {
                echo json_encode(array(
                    'success'   => 0,
                    'error'     => 'You don\'t have valid access token!',
                    'code'      => 2
                ));
                exit;
            }

            return $fn;
        }
    }
?>
