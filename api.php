<?php

require_once 'init.php';

Epi::init('route');

require_once 'token_checker.php';

getRoute()->post('/login', 'login');
getRoute()->get('/student', 'getStudentInfo');
getRoute()->post('/student/apply', 'apply');
getRoute()->post('/student/bank-account', 'updateBankAccount');
getRoute()->put('/student/grade', 'updateStudentGrade');
getRoute()->get('/student/ranking', 'getStudentRanking');
getRoute()->get('/student/payments', 'getStudentPayments');
getRoute()->run();

function login() {
    if ($_POST['username'] == 'hackfmi' && $_POST['password'] == 'hackfmi_pass') {
        $tokens_delete = "
			DELETE FROM token_service.tokens WHERE faculty_number=?;";
        $tokens_insert = "
			INSERT INTO token_service.tokens (token, granted, faculty_number)
			VALUES (?, now(), ?);
		";

        $token = md5($_POST['username'] . $_POST['password'] . uniqid(rand(), true));

        $db = getDB();
        $statement = $db->prepare($tokens_delete) or database_error();
        $statement->execute(array(65432)) or database_error();
        $statement = $db->prepare($tokens_insert) or database_error();
        $statement->execute(array($token, 65432)) or database_error();

        echo json_encode(array(
            'student' => array(
                'primary' => array(
                    'access_token' => '65432-' . $token,
                    'fn' => 65432,
                    'major' => "Софтуерни технологии",
        ))));

        exit();
    } else if ($_POST['username'] == 'hackfmi2' && $_POST['password'] == 'hackfmi_pass') {
        $tokens_delete = "
			DELETE FROM token_service.tokens WHERE faculty_number=?;";
        $tokens_insert = "
			INSERT INTO token_service.tokens (token, granted, faculty_number)
			VALUES (?, now(), ?);
		";

        $token = md5($_POST['username'] . $_POST['password'] . uniqid(rand(), true));
        $token2 = md5($_POST['username'] . $_POST['password'] . uniqid(rand(), true));

        $db = getDB();
        $statement = $db->prepare($tokens_delete) or database_error();
        $statement->execute(array(12345)) or database_error();
        $statement->closeCursor();
        $statement->execute(array(111222)) or database_error();
        $statement = $db->prepare($tokens_insert) or database_error();
        $statement->execute(array($token, 12345)) or database_error();
        $statement->closeCursor();
        $statement->execute(array($token2, 111222)) or database_error();

        echo json_encode(array(
            'student' => array(
                'primary' => array(
                    'access_token' => '12345-' . $token,
                    'fn' => 12345,
                    'major' => "Софтуерни технологии",
                ),
                'secondary' => array(
                    'access_token' => '111222-' . $token2,
                    'fn' => 111222,
                    'major' => "Софтуерни ьяаьяьяа",
                )
            )
                )
        );

        exit();
    } else {
        echo json_encode(array(
            "error" => 1,
            "err_msg" => "Wrong username or password. Please try again.",
        ));

        exit();
    }
}

function getStudentInfo() {
    $fn = check_token();

    $student_info = "SELECT
        app.id as id,
        app.first_name as name,
        app.second_name as second_name,
        app.last_name as last_name,
        app.faculty_number as fn,
        deg.name as degree,
        maj.name as major,
        app.avg_grade as average_grade,
        sem.name as semester,
        app.course as course,
        stu.personal_number as personal_number
        FROM
        application_data.applications as app,
        application_data.degrees as deg,
        application_data.majors as maj,
        application_data.semesters as sem,
        application_data.students as stu
        WHERE
        app.faculty_number = ?
        AND app.deleted = false
        AND stu.id = app.student_id
        AND deg.id = app.degree_id
        AND maj.id = app.major_id
        AND sem.id = app.semester_id";

    $db = getDB();
    $statement = $db->prepare($student_info) or database_error();
    $statement->execute(array($fn)) or database_error();
    if ($statement->rowCount() == 0) {
        $select_query = "
            SELECT 1 FROM application_data.students
            WHERE username = ? AND NOT deleted";

        $select_statement = $db->prepare($select_query) or database_error();
        $select_statement->execute(array('s12345')) or database_error();
        if ($select_statement->rowCount() == 0) {
            $insert_query = "
            INSERT INTO application_data.students
            (username, susi_id, personal_number)
            VALUES (?, ?, ?)";

            $insert_statement = $db->prepare($insert_query) or database_error();
            $insert_statement->execute(array('s12345', 12345, "3215009876")) or
                    database_error();
        }

        echo json_encode(array(
                'applied' => 0,
                'name' => "Петър",
                'second_name' => "Георгиев",
                'last_name' => "Тодоров",
                'fn' => 12345,
                'degree' => 'Бакалавър',
                'major' => "Софтуерно инженерство",
                'average_grade' => 423,
                'semester' => 2,
                'course' => 2,
                'personal_number' => "3215009876",
                "priority" => 1
        ));

        exit();
    } else {
        $info = $statement->fetch(PDO::FETCH_ASSOC);

        $student_bank_account = "
            SELECT bic, iban as account_number
            FROM bank_accounts.bank_accounts
            WHERE application_id = ? AND NOT deleted";

        $statement = $db->prepare($student_bank_account) or database_error();
        $statement->execute(array($info['id'])) or database_error();
        if ($statement->rowCount() == 0) {
            $acc = array('bic' => '', 'account_number' => '');
        } else {
            $acc = $statement->fetch(PDO::FETCH_ASSOC);
        }

        $reslult = array_merge($info, $acc, array(
            'rating' => 3,
            'overall' => 40,
            'applied' => 1));
        unset($reslult['id']);
        echo json_encode($reslult);
        exit();
    }
}

function updateBankAccount($id) {
    $fn = check_token();

    $update_bank_info_query = "
		UPDATE
			bank_accounts.bank_accounts
		SET
			iban=?,
			bic=?
		WHERE
			application_id = (
				SELECT
					id
				FROM
					application_data.applications
				WHERE faculty_number = ?
				AND deleted=false
			)
	";

    $db = getDB();
    $statement = $db->prepare($update_bank_info_query) or database_error();
    $statement->execute($_REQUEST['bank_account'], $_REQUEST['bic'], $fn) or database_error();

    echo json_encode(array(
        'success' => 1
    ));
    exit();
}

function updateStudentGrade($id) {
    if ($id == '12345') {
        echo json_encode(array(
            'average_grade' => 5.34,
        ));
    } else {
        echo json_encode(array(
            'error' => 0,
            'err_msg' => 'Unknown student',
        ));
    }
}

function getStudentRanking() {
    ranking();
}

function getStudentPayments() {
    $fn = check_token();
    $query = "
		SELECT
		EXTRACT (EPOCH FROM pay.payment_date) * 1000 as date,
		pay.payment_amount::float / 100 as amount,
		pay.month as month
		FROM
		application_data.monthly_payments as pay,
		application_data.winners as win,
		application_data.applications as app
		WHERE
		app.faculty_number = ?
		AND win.application_id = app.id
		AND pay.winner_id = win.id
		";

    $db = getDB();
    $statement = $db->prepare($query) or database_error();
    $statement->execute(array($fn)) or database_error();
    if ($statement->rowCount() == 0) {
        echo json_encode(array(
            'error' => 0,
            'err_msg' => 'Unknown student',
        ));

        exit();
    } else {
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode(array_map(function ($payment) {
                            return array('payment' => $payment);
                        }, array_values($result)));
        exit();
    }
}

function ranking() {
    $fn = check_token();
    $db = getDB();

    $quota_and_major_query = "
        SELECT
        quo.quote as quote,
        quo.primary_major as major,
        maj.name as major_name
        FROM
        application_data.quotes as quo,
        application_data.applications as app,
        application_data.majors as maj
        WHERE app.faculty_number = ?
        AND app.major_id = quo.secondary_major
        AND maj.id = app.major_id";

    $quota_and_major_statement = $db->prepare($quota_and_major_query) or
            database_error();

    $quota_and_major_statement->execute(array($fn)) or
            database_error();

    if ($quota_and_major_statement->rowCount() == 0) {
        echo json_encode(array(
            'success' => 0,
            'err_msg' => 'Няма дефинирана квота за тази специалност'
        ));

        exit();
    }

    $result = $quota_and_major_statement->fetch(PDO::FETCH_ASSOC);
    $quote = $result['quote'];
    $major = $result['major'];
    $major_name = $result['major_name'];

    $min_grade_query = "
        SELECT
            a.avg_grade as grade
        FROM (
                SELECT
                    app.avg_grade
                FROM
                    application_data.applications as app,
                    application_data.quotes as quo
                WHERE quo.primary_major = ?
                AND app.major_id = quo.secondary_major
                ORDER BY app.avg_grade DESC
                LIMIT ?
            ) as a
        ORDER BY a.avg_grade ASC
        LIMIT 1";

    $min_grade_statement = $db->prepare($min_grade_query) or
            database_error();

    $min_grade_statement->execute(array($major, $quote)) or
            database_error();

    if ($min_grade_statement->rowCount() == 0) {
        echo json_encode(array(
            'success' => 0,
            'err_msg' => "Няма кандидатствали студенти"
        ));

        exit();
    }

    $result = $min_grade_statement->fetch(PDO::FETCH_ASSOC);
    $min_grade = $result['grade'];

    $campaign_query = "
        SELECT
        sem.id as semester,
        camp.academic_year as year
        FROM
        application_data.semesters as sem,
        application_data.campaigns as camp
        WHERE
        NOT camp.deleted
        AND sem.id = camp.semester_id";

    $campaign_statement = $db->prepare($campaign_query) or database_error();
    $campaign_statement->execute(array()) or database_error();

    if ($campaign_statement->rowCount() != 1) {
        echo json_encode(array(
            'success' => 0,
            'err_msg' => "Не можем да определим активната кампания"
        ));

        exit();
    }

    $result = $campaign_statement->fetch(PDO::FETCH_ASSOC);
    $semester = $result['semester'];
    $year = $result['year'];

    $ranking_query = "
        SELECT
            app.id
        FROM
            application_data.applications as app,
            application_data.quotes as quo
        WHERE quo.primary_major = ?
        AND app.major_id = quo.secondary_major
        AND app.avg_grade >= ?
        ORDER BY app.avg_grade DESC";

    $ranking_statement = $db->prepare($ranking_query) or database_error();
    $ranking_statement->execute(array($major, ($min_grade > 400) ? $min_grade : 400)) or database_error();
    $overall = $ranking_statement->rowCount();

    $ranking_query = "
        SELECT
            app.first_name as name,
            app.last_name as lastname,
            deg.name as degree,
            app.faculty_number as fn,
            app.avg_grade as average_grade,
            app.course as course
        FROM
            application_data.applications as app,
            application_data.quotes as quo,
            application_data.degrees as deg
        WHERE quo.primary_major = ?
        AND app.major_id = quo.secondary_major
        AND app.avg_grade >= 4
        AND deg.id = app.degree_id
        ORDER BY app.avg_grade DESC";

    $ranking_statement = $db->prepare($ranking_query) or database_error();
    $ranking_statement->execute(array($major)) or database_error();

    $result = $ranking_statement->fetchAll(PDO::FETCH_ASSOC);

    $json = array(
        'major' => $major_name,
        'overall' => $overall,
        'semester' => $semester,
        'year' => $year,
        'students' => $result
    );

    echo json_encode($json);
    exit();
}

function apply() {
    $fn = check_token();
    $db = getDB();

    $select_query = "SELECT 1 FROM application_data.applications
        WHERE faculty_number = ? AND NOT deleted";

    $select_statement = $db->prepare($select_query) or database_error();
    $select_statement->execute(array($fn)) or database_error();

    if ($select_statement->rowCount() == 0) {
        $app_query = "
        INSERT INTO application_data.applications
        (first_name, second_name, last_name, faculty_number, major_id,
        degree_id, avg_grade, semester_id, priority, course, campaign_id,
        student_id)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        $bank_query = "
        INSERT INTO bank_accounts.bank_accounts
        (iban, bic, application_id)
        VALUES (?, ?, (
            SELECT id FROM application_data.applications
            WHERE faculty_number = ? AND NOT deleted))";


        $app_statement = $db->prepare($app_query) or database_error();
        $app_statement->execute(array(
                    "Петър",
                    "Георгиев",
                    "Тодоров",
                    12345,
                    1,
                    1,
                    423,
                    2,
                    1,
                    2,
                    1,
                    3
                )) or database_error();

        $iban = $_POST['bank_account'];
        $bic = $_POST['bic'];
        $bank_statement = $db->prepare($bank_query) or database_error();
        $bank_statement->execute(array($iban, $bic, $fn)) or database_error();

        echo json_encode(array(
            'success' => 1,
        ));

        exit();
    } else {
        echo json_encode(array(
            'success' => 0,
            'err_msg' => 'Студентът вече е кандидатствал за стипендия'
        ));

        exit();
    }
}
?>
