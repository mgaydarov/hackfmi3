api = '/student/ranking?access_token='

$(document).ready(function () {	
	student_access_token = sessionStorage.getItem("access_token");
	$('#rating').click(function () {
		$.ajax({
			type: 'GET',
			url: api + student_access_token
		})
			.done( function (data) {
				sessionStorage.setItem("ranking", data);
				window.location.href = "rating.html";
			})
				.fail(function() {
					alert( "error" );
			});
	});
}); 
