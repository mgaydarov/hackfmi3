var api_apply = '/student/apply';
var first = true;
var access_token;

function load_profile_table () {
	result = jQuery.parseJSON(sessionStorage.getItem("student"));
	access_token = sessionStorage.getItem("access_token");
	append_profile_row(result);
}

function append_profile_row (student) {
	if (student.applied){
		$('#profile').append('<thead><tr><th>Място в класацията: '+ student.rating + '/' + student.overall +'</th><th><button type="button" class="btn btn-sm btn-info rightBtn" id="editBtn">Редактирай</button></th></tr></thead>');
	}
	
	content = '<tbody>' +
	'<tr>' +
	'	<td>' + 'Име' + '</td>' +
	'	<td>' + student['name'] + '</td>' +
	'</tr>' +
	'<tr>' +
	'	<td>' + 'Презиме' + '</td>' +
	'	<td>' + student['second_name'] + '</td>' +
	'</tr>' +
	'<tr>' +
	'	<td>' + 'Фамилия' + '</td>' +
	'	<td>' + student['last_name'] + '</td>' +
	'</tr>' +
	'<tr>' +
	'	<td>' + 'Степен' + '</td>' +
	'	<td>' + student['degree'] + '</td>' +
	'</tr>' +
	'<tr>' +
	'	<td>' + 'Специалност' + '</td>' +
	'	<td>' + student['major'] + '</td>' +
	'</tr>' +
	'<tr>' +
	'	<td>' + 'Факултетен номер' + '</td>' +
	'	<td>' + student['fn'] + '</td>' +
	'</tr>' +
	'<tr>' +
	'	<td>' + 'Успех' + '</td>' +
	'	<td id="grade">' + student['average_grade'] / 100 + '</td>' +
	'</tr>'  +
	'<tr>' +
	'	<td>' + 'Курс' + '</td>' +
	'	<td>' + student['course'] + '</td>' +
	'</tr>' +
	'<tr>' +
	'	<td>' + 'ЕГН' + '</td>' +
	'	<td>' + student['personal_number'] + '</td>' +
	'</tr>';
	
	if (!student.applied)
	{
		content = content +
		'<tr>' +
		'	<td id="bkal">' + 'Банкова сметка <span id="star">*</span>' + '</td>' +
		'	<td id="bkar"><form><input type="text" name="bank_account" id="save_bak"></form></td>' +
		'</tr>' +
		'<tr>' +
		'	<td id="bicl">' + 'BIC <span id="star">*</span>' + '</td>' +
		'	<td id="bicr"><form><input type="text" name="bank_id" id="save_bic"></form></td>' +
		'</tr>' + '</tbody>' + 
		' <tfoot><tr><td><span id="star">* Задължително поле.</span></td></tr></tfoot>';
	} else 
	{
		content = content + '<tr>' +
		'	<td id="bkal">' + 'Банкова сметка' + '</td>' +
		'	<td id="bkar">' + student['account_number'] + '</td>' +
		'</tr>' +
		'<tr>' +
		'	<td id="bicl">' + 'BIC' + '</td>' +
		'	<td id="bicr">' + student['bic'] + '</td>' +
		'</tr>' + '</tbody>';
	}
	
	$('#profile').append(content);
	
	if (!student.applied)
	{
		$('.content').append('<button type="button" class="btn btn-lg btn-info centerBtn" id="applyBtn">Кандидатствай</button>');
	} 
}

function student_apply (bank_account, bic) {
	$.ajax({
			type: 'POST',
			url: api_apply,
			data: {
				bank_account: bank_account,
				bic: bic, 
				access_token: access_token
			}
		})
			.done( function (data) {
			})
			.fail(function() {
				alert( "error" );
			});		
}

function validateIBAN (iban) {
	var valid = true;
	if (!iban) {
		$('#save_bak').addClass('error');
		valid = false;
	} else {
		$('#save_bak').removeClass('error');
	}
	
	return valid;
}

function validateBIC(bic) {
	var valid = true;
	if (!bic) {
		$('#save_bic').addClass('error');
		valid = false;
	} else {
		$('#save_bic').removeClass('error');
	}
	
	return valid;
}

$(document).ready(function () {	
	load_profile_table();
	
	$('#applyBtn').click(function(e) {
	
		if (validateIBAN(document.getElementById('save_bak').value) & validateBIC(document.getElementById('save_bic').value)) {
			student_apply(document.getElementById('save_bak').value, document.getElementById('save_bic').value);
		} else {
			return;
		}
	
		$(this).toggle();
		$('#profile').append('<thead><tr><th>Място в класацията: 10/30</th><th><button type="button" class="btn btn-sm btn-info rightBtn" id="editBtn">Редактирай</button></th></tr></thead>');
		document.getElementById('bkal').innerHTML = 'Банкова сметка';
		document.getElementById('bicl').innerHTML = 'BIC';
		document.getElementById('bkar').innerHTML = document.getElementById('save_bak').value;
		document.getElementById('bicr').innerHTML = document.getElementById('save_bic').value;
		
		$('#editBtn').click(function(e) {
			
			if (first) 
			{
				var val1 = document.getElementById('bkar').innerHTML;
				var val2 = document.getElementById('bicr').innerHTML;
				
				document.getElementById('bkal').innerHTML = 'Банкова сметка <span id="star">*</span>';
				document.getElementById('bicl').innerHTML = 'BIC <span id="star">*</span>';
				document.getElementById('bkar').innerHTML = '<form><input type="text" name="bank_account" value=' + val1 + ' id="save_bak"></form>';		
				document.getElementById('bicr').innerHTML = '<form><input type="text" name="bank_id" value=' + val2 + ' id="save_bic"></form>';
				
				first=false;								
			}
			
			$(this).toggle();
			if (!document.getElementById('saveBtn'))
			{
				$('.content').append('<button type="button" class="btn btn-lg btn-info centerBtn" id="saveBtn">Запази</button>');			
				$('#saveBtn').click(function(e) {
					var val3 = document.getElementById('save_bak').value;
					var val4 = document.getElementById('save_bic').value;
					if (validateIBAN(val3) & validateBIC(val4)) {
						document.getElementById('bkal').innerHTML = 'Банкова сметка';
						document.getElementById('bicl').innerHTML = 'BIC';
						document.getElementById('bkar').innerHTML = val3;
						document.getElementById('bicr').innerHTML = val4;
						first = true;
					} else {
						return;
					}		

					$(this).toggle();
					$('#editBtn').toggle();
					$('#updateBtn').toggle();					
				});
			} else {
				$('#saveBtn').toggle();
			}	
			
			if (!document.getElementById('updateBtn'))
			{
				var val3 = document.getElementById('grade').innerHTML;
				document.getElementById('grade').innerHTML = '<span style="float:left;">' + val3 + '</span><button type="button" class="btn btn-sm btn-info rightBtn" id="updateBtn">Обнови</button>';
				
				$('#updateBtn').click(function(e) {
				});
			} else {
				$('#updateBtn').toggle();
			}
		});
	});
	
	$('#editBtn').click(function(e) {
			
			if (first) 
			{
				var val1 = document.getElementById('bkar').innerHTML;
				var val2 = document.getElementById('bicr').innerHTML;
				
				document.getElementById('bkal').innerHTML = 'Банкова сметка <span id="star">*</span>';
				document.getElementById('bicl').innerHTML = 'BIC <span id="star">*</span>';
				document.getElementById('bkar').innerHTML = '<form><input type="text" name="bank_account" value=' + val1 + ' id="save_bak"></form>';		
				document.getElementById('bicr').innerHTML = '<form><input type="text" name="bank_id" value=' + val2 + ' id="save_bic"></form>';
				
				first=false;								
			}
			
			$(this).toggle();
			if (!document.getElementById('saveBtn'))
			{
				$('.content').append('<button type="button" class="btn btn-lg btn-info centerBtn" id="saveBtn">Запази</button>');			
				$('#saveBtn').click(function(e) {
					var val3 = document.getElementById('save_bak').value;
					var val4 = document.getElementById('save_bic').value;
					if (validateIBAN(val3) & validateBIC(val4)) {
						document.getElementById('bkal').innerHTML = 'Банкова сметка';
						document.getElementById('bicl').innerHTML = 'BIC';
						document.getElementById('bkar').innerHTML = val3;
						document.getElementById('bicr').innerHTML = val4;
						first = true;
					} else {
						return;
					}		

					$(this).toggle();
					$('#editBtn').toggle();
					$('#updateBtn').toggle();					
				});
			} else {
				$('#saveBtn').toggle();
			}	
			
			if (!document.getElementById('updateBtn'))
			{
				var val3 = document.getElementById('grade').innerHTML;
				document.getElementById('grade').innerHTML = '<span style="float:left;">' + val3 + '</span><button type="button" class="btn btn-sm btn-info rightBtn" id="updateBtn">Обнови</button>';
				
				$('#updateBtn').click(function(e) {
				});
			} else {
				$('#updateBtn').toggle();
			}
		});
});
