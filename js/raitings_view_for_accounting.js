var api = 'ocenki.php';

function load_rating_table ( json ) {
	//alert("one last time");
	// date used for test purposes;
	result = jQuery.parseJSON('{"major": "Компютърни науки","overall": 7,"semester": "зимен","year": "2013/2014","students":[{"name": "Vassilena","lastname": "Slaveva","degree" : "bachelor","fn": "126521","average_grade": "6.00","course": "10","rating": 1},{"fn": 5002,"degree": true,"course": 0,"average_grade": 102.9381,"name": "Lois","lastname": "Hoover","rating": 1},{"fn": 539,"degree": false,"course": 43,"average_grade": 92.536,"name": "Allison","lastname": "Best","rating": 2},{"fn": 8837,"degree": false,"course": 32,"average_grade": 162.0758,"name": "Penny","lastname": "Hobbs","rating": 3},{"fn": 4238,"degree": false,"course": 19,"average_grade": 89.4855,"name": "Tommy","lastname": "Dyer","rating": 4},{"fn": 8214,"degree": true,"course": 35,"average_grade": 172.1549,"name": "Joe","lastname": "Berg","rating": 5},{"fn": 8711,"degree": true,"course": 33,"average_grade": 100.4233,"name": "Adam","lastname": "Scarborough","rating": 6},{"fn": 452,"degree": false,"course": 7,"average_grade": 28.977,"name": "Paige","lastname": "Cooke","rating": 7},{"fn": 1713,"degree": false,"course": 35,"average_grade": 179.7353,"name": "Nina","lastname": "Lopez","rating": 8},{"fn": 2057,"degree": false,"course": 44,"average_grade": 95.1792,"name": "Anna","lastname": "Hewitt","rating": 9},{"fn": 6695,"degree": false,"course": 13,"average_grade": 41.3848,"name": "Hannah","lastname": "Bowles","rating": 10}]}');
	
	// Production code
	// result = jQuery.parseJSON(json);
	
	// Generate information for the table
	var information_for_the_table = 'Класация за'
		+ ' специалност '			+ '<b>' + result.major + '</b>'
		+ ' с квота '				+ '<b>' + result.overall + '</b>' 
		+ ' за '					+ '<b>' + result.semester + '</b>' + ' семестър '
		+ ' на академична година '	+ '<b>' + result.year + '</b>' ;
	
	// initialize the table;
	var rating_table = '';
	
	// Header of the table;
	rating_table = rating_table + '<thead>'+
		'<tr>'+
		'	<th>' + 'Позиция' + '</th>' +
		'	<th>' + 'Име' + '</th>' +
		'	<th>' + 'Фамилия' + '</th>' +
		'	<th>' + 'Степен на образование' + '</th>' +
		'	<th>' + 'Факултетен номер' + '</th>' +
		'	<th>' + 'Среден успех' + '</th>' +
		'	<th>' + 'Курс' + '</th>' +
		'</tr>' + '</thead>' + '<tbody>'; 
		
	// Variable used for temporary storage of generated row;
	var row_to_be_added = '';
	
	// Get the number of payments
	var array_length = result.students.length ;
	
	// For each payment
	for( var i=0; i < array_length; i++ )
	{
		// generate new row of payment
		row_to_be_added = append_student_row( result.students[i], result.overall );
				
		// add the row to the table
		rating_table = rating_table.concat( row_to_be_added );
		// Debug message; Shows the content of payment_table variable
		//alert(payment_table);
	}
	
	// Close the table
	rating_table = rating_table.concat('</tbody>');
	/* */
	// Inject the table in the HTML
	$('#rating_table').append(rating_table);
	$('#information_text').append(information_for_the_table);
	
	//alert("one last time");
}	

function append_student_row ( student, quota) {
	
	var student_row = '';
	
	// format the rows so the winners appear in different color
	if ( student['rating'] < quota )
	{
		student_row = '<tr class="winner">';
	}
	else
	{
		student_row = '<tr class="loser">';
	}
		
	// generate the data row
	student_row = 	student_row +
		'	<td>' + student['rating'] + '</td>' +
		'	<td>' + student['name'] + '</td>' +
		'	<td>' + student['lastname'] + '</td>' +
		'	<td>' + student['degree'] + '</td>' +
		'	<td>' + student['fn'] + '</td>' +
		'	<td>' + student['average_grade'] + '</td>' +
		'	<td>' + student['course'] + '</td>' +
		'</tr>';
	
	return student_row;
}

// get some better formating for the time
// makes 0 to appear as 00
//		 1 to appear as 01
// 		 2 to appear as 02
// 		 3 to appear as 03 and so on to 9 - 09
function format_time_property ( time_value ) 
{

	if ( time_value < 10 )
		return '0' + time_value;
		
	return time_value.toString();
}

function display_rating_menu()
{
	// Button for selecting faculty
	var select_faculty_button = '<input name="select_faculty" id="selected_faculty" class="input-small" placeholder="Факултет" list="faculties"></input>';

	// List of VALID options
	select_faculty_button = select_faculty_button +
		'<datalist id="faculties">' +
		'	<option value="Internet Explorer">' +
		'	<option value="Firefox">' +
		'	<option value="Chrome">' +
		'	<option value="Opera">' +
		'	<option value="Safari">' +
		'</datalist>';
	
	// Button for selecting major
	var select_major_button = '<input name="select_major" id="selected_major" class="input-small" placeholder="Специалност" list="majors"></input>';
	
	select_major_button = select_major_button +
		'<datalist id="majors">' +
		'	<option value="Coding Monkey">' +
		'	<option value="QA">' +
		'	<option value="Dev">' +
		'	<option value="Engineer">' +
		'	<option value="Architect">' +
		'</datalist>';
		
	var menu = 	'<tr>' +
				'	<td>' +	'Избери факултет:' +
				'	</td>' +
				'	<td>' +
						select_faculty_button + '</form>' +
				'	</td>' +
				'</tr>' +
				'<tr>' +
				'	<td>' +
						'Избери специалност:' + '</td>' +
				'	<td>' +
						select_major_button + '</td>' +
				'</tr>';
				
	$('#rating_menu').append(menu);
}

$(document).ready(function () {	
	
	// show menu:
	display_rating_menu();

	$( "#showRatingBtn" ).click(function() {
		//alert( "Handler for .click() called." );
		
		// collect data
		
		// add the table;
		var json = "";
		load_rating_table( json );
	});
	
	
}); 