var api = '/login';

function validateName(bic) {
	var valid = true;
	if (!bic) {
		$('#susi_name').addClass('error');
		valid = false;
	} else {
		$('#susi_name').removeClass('error');
	}
	
	return valid;
}

function validatePass(bic) {
	var valid = true;
	if (!bic) {
		$('#user_password').addClass('error');
		valid = false;
	} else {
		$('#user_password').removeClass('error');
	}
	
	return valid;
}

$(document).ready(function () {
	$('form').submit(false);
	$('#loginBtn').click(function(e) {
		if (validateName(document.getElementById('susi_name').value) & validatePass(document.getElementById('user_password').value)) {
			$.ajax({
				type: 'POST',
				url: api,
				data: {
					action: 'login',
					username: document.getElementById('susi_name').value,
					password: document.getElementById('user_password').value
				}
			})
				.done( function (data) {
					sessionStorage.setItem("priority", data);
					window.location.href = "secondMajor.html";
				})
				.fail(function() {
					alert( "error" );
				});
		} else {
			return;
		}
	});
});