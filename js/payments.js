var api = 'ocenki.php';

function load_payment_table () {
	// date used for test purposes;
	result = jQuery.parseJSON('{"payment":[ {"date": 1301090400,"amount": "10.05","month": "Март"}, {"date": 8236856842,"amount": "13.05","month": "Април"}]}');
	
	/*
	// This ajax must be edited !
	$.ajax({
		type: 'POST',
		url: api,
		data: {
			action: 'load_profile_table'
		}
	})
		.done( function (data) {
			$('#profile').html('');

			try {
				response = jQuery.parseJSON(data);
			} catch (e) {
				response = '';
			}
			$.each(response, append_profile_row);
		});*/
	
	
	// Header of the table;
	var payment_table = '<thead style="display:none">'+'<tr>'+
		'	<th>' + 'Дата' + '</th>' +
		'	<th>' + 'Сума' + '</th>' +
		'	<th>' + 'Месец' + '</th>' +
		'</tr>' + '</thead>' + '<tbody style="display:none">'; 
		
	// Variable used for temporary storage of generated row;
	var row_to_be_added = '';
	
	// Get the number of payments
	var array_length = result.payment.length ;
	
	// For each payment
	for( var i=0; i < array_length; i++ )
	{
		// generate new row of payment
		row_to_be_added = append_payment_row( result.payment[i] );
		// add the row to the table
		payment_table = payment_table.concat( row_to_be_added );
		// Debug message; Shows the content of payment_table variable
		//alert(payment_table);
	}
	
	// Close the table
	payment_table = payment_table.concat('</tbody>');
	
	// Inject the table in the HTML
	$('#payment').append(payment_table);
}	

function append_payment_row (payment) {
	// convert timestamp to date
	var date_of_payment = new Date ( payment['date'] * 1000 );
	
	// Array with months
	var months = ['Януари','Фебруари','Март','Април','Май','Юни','Юли','Август','Септември','Октомври','Ноември','Декември'];
	
	// Format the month so it's human readable
		// That's a bit tricky - getMonth returns 0-11 which is number
	var month = months[date_of_payment.getMonth()]; 
	
	// get the hour digits
	var hours = date_of_payment.getHours();
	var minutes = date_of_payment.getMinutes();
	var seconds = date_of_payment.getSeconds();
	
	// Apply some formating - this will make the hour output more pretty
	hours = format_time_property( hours );
	minutes = format_time_property ( minutes );
	seconds = format_time_property ( seconds );
	
	// Foramat the date so it's readable for humans
	date_of_payment = date_of_payment.getFullYear() + ' ' +
					
					month + ' ' +
					date_of_payment.getDay() + ' ' + 				
					hours + ':' + 
					minutes + ':' + 
					seconds;
	
	// generate the data row
	payment_row ='<tr>' +
	'	<td>' + 
		// convert the timestamp to date
		date_of_payment + '</td>' +
	'	<td>' + payment['amount'] + ' лв' + '</td>' +
	'	<td>' + payment['month'] + '</td>' +
	'</tr>';
	
	return payment_row;
}

// get some better formating for the time
// makes 0 to appear as 00
//		 1 to appear as 01
// 		 2 to appear as 02
// 		 3 to appear as 03 and so on to 9 - 09
function format_time_property ( time_value ) 
{

	if ( time_value < 10 )
		return '0' + time_value;
		
	return time_value.toString();
}

$(document).ready(function () {	
	load_payment_table();
}); 