var api_info = '/student?access_token=';
var student_access_token_primare;
var student_access_token_secondary;

function get_student_info (student_access_token) {
	sessionStorage.setItem("access_token", student_access_token);
	$.ajax({
		type: 'GET',
		url: api_info + student_access_token
	})
		.done( function (data) {
			sessionStorage.setItem("student", data);
			window.location.href = "index.html";
		})
			.fail(function() {
				alert( "error" );
		});
}

function load_fns_table () {
	result = jQuery.parseJSON(sessionStorage.getItem("priority"));
	append_fns(result.student);
}

function append_fns (student) {
	student_access_token_primare = student.primary.access_token;
	if (student.secondary) {
		content = '<thead>' +
		'<tr>' +
		' <th>Факултетен номер</td>' +
		' <th>Специалност</td>' +
		'</tr>' +
		'</thead>' +
		'<tbody>' +
		'<tr class="priority_primary">' +
		' <td>' + student.primary.fn + '</td>' +
		' <td>' + student.primary.major + '</td>' +
		'</tr>' +
		'<tr class="priority_secondaty">' +
		' <td>' + student.secondary.fn + '</td>' +
		' <td>' + student.secondary.major + '</td>' +
		'</tr></a>' + '</tbody>';
		$('#fns').append(content);
		student_access_token_secondary = student.secondary.access_token;
	} else {
		get_student_info(student_access_token_primare);
	}
}
  
$(document).ready(function () {	
	load_fns_table();
	
	$('.priority_primary').click(function(e) {
		get_student_info(student_access_token_primare);
	});
	$('.priority_secondaty').click(function(e) {
		get_student_info(student_access_token_secondary);
	});
	$('.priority_primary').css("cursor", "pointer");
	$('.priority_secondaty').css("cursor", "pointer");
});
